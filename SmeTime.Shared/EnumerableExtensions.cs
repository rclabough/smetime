﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmeTime.Shared
{
    public static class EnumerableExtensions
    {
        public static void AddRange<T>(this ICollection<T> items, T[] toAdd)
        {
            foreach (var item in toAdd)
            {
                items.Add(item);
            }
        }

        public static void AddRange<T>(this ICollection<T> items, ICollection<T> toAdd)
        {
            foreach (var item in toAdd)
            {
                items.Add(item);
            }
        }

        public static string ToFormattedString(this IEnumerable<string> items)
        {
            StringBuilder sb = new StringBuilder();
            foreach(var item in items.OrderBy(k => k))
            {
                sb.Append($"- {item}\n");
            }
            return sb.ToString();
        }
    }
}
