﻿using System;

namespace SmeTime.Shared
{
    public static class ExceptionExtensions
    {
        public static string ToFormattedMessage(this Exception exception)
        {
            return $"Exception: {exception.Message} | {exception.StackTrace}";
        }
    }
}
