﻿using FluentValidation.Results;
using System;
using System.Text;

namespace SmeTime.Shared.Exceptions
{
    public class ValidationException : Exception
    {
        public ValidationResult ValidationResult { get; set; }

        public ValidationException(ValidationResult validationResult) : base(BuildMessage(validationResult))
        {
            
        }

        private static String BuildMessage(ValidationResult validationResult)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("The following validation failures were found: ");
            foreach (var error in validationResult.Errors)
            {
                sb.AppendLine("\t" + error.ErrorMessage);
            }
            return sb.ToString();
        }
    }
}
