﻿using FluentValidation.Results;
using SmeTime.Shared.Exceptions;
using System;

namespace SmeTime.Shared
{
    public static class ValidationResultExtensions
    {
        public static Exception ToException(this ValidationResult validationResult)
        {
            return new ValidationException(validationResult);
        }
    }
}
