﻿using FluentValidation.Results;
using SmeTime.Logic.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmeTime.Logic.Interfaces
{
    public interface ISmeService
    {
        Task<ValidationResult> RegisterTech(SlackUserModel slackUserModel, IEnumerable<string> tech);

        Task<ValidationResult> AddTech(SlackUserModel slackUserModel, IEnumerable<string> tech);
        
        Task<SlackUserTechModel> GetTechForUser(SlackUserModel slackUseModel);

        Task<IEnumerable<string>> GetUsersWhoKnow(string tech);

        Task<IEnumerable<string>> ListTech();
    }
}
