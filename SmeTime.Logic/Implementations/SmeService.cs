﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentValidation.Results;
using SmeTime.Logic.Interfaces;
using SmeTime.Logic.Models;
using SmeTime.Data;
using SmeTime.Data.Entities;
using SmeTime.Data.Commanders;
using System.Linq;
using System.Data.Entity;
using System.Linq.Expressions;

namespace SmeTime.Logic.Implementations
{

    public class SmeService : ISmeService
    {
        private readonly ITechCommander _techCommander;
        private readonly ISlackUserCommander _slackUserCommander;

        private readonly IReader<Tech> _techReader;
        private readonly IReader<SlackUser> _slackUserReader;

        public static Expression<Func<Tech, string>> FnTechToString = k => k.Name;

        public SmeService(ITechCommander techCommander, ISlackUserCommander slackUserCommander, IReader<Tech> techReader, IReader<SlackUser> slackUserReader)
        {
            _techCommander = techCommander;
            _slackUserCommander = slackUserCommander;
            _techReader = techReader;
            _slackUserReader = slackUserReader;
        }
        
        public async Task<SlackUserTechModel> GetTechForUser(SlackUserModel slackUseModel)
        {
            var slackUser = await _slackUserReader.Query().Where(k => k.UserId == slackUseModel.UserId).FirstOrDefaultAsync();
            if (slackUser == null)
            {
                throw new InvalidOperationException($"Could not find user {slackUseModel.UserName}");
            }

            var tech = slackUser.KnownTech.Select(k => k.Name);
            return new SlackUserTechModel
            {
                KnownTech = tech
            };
        }

        public async Task<ValidationResult> RegisterTech(SlackUserModel slackUserModel, IEnumerable<string> tech)
        {
            var techArr = tech.Where(k => !string.IsNullOrWhiteSpace(k)).Select(k => k.ToLower()).ToArray();

            var techEntities = await _techReader.Query()
                .Where(k => techArr.Contains(k.Name))
                .ToListAsync();

            foreach (var str in techArr)
            {
                if (!techEntities.Any(k => k.Name == str))
                {
                    var techId = Guid.NewGuid();
                    var innerResult = await _techCommander.Add(techId)
                        .SetName(str)
                        .Apply();
                    var entity = await _techReader.Query().FirstOrDefaultAsync(k => k.TechId == techId);
                    techEntities.Add(entity);
                }
            }

            var techIds = techEntities.Select(k => k.TechId).ToList();

            var existing = _slackUserReader.Query().FirstOrDefault(k => k.UserName == slackUserModel.UserName
                && k.UserId == slackUserModel.UserId
                && k.Token == slackUserModel.Token
                && k.TeamId == slackUserModel.TeamId);
            var cmd = existing == null ? _slackUserCommander.Create() : _slackUserCommander.Update(existing.SlackUserId);

            var result = await cmd
                .SetTeamId(slackUserModel.TeamId)
                .SetToken(slackUserModel.Token)
                .SetUserId(slackUserModel.UserId)
                .SetUserName(slackUserModel.UserName)
                .SetKnownTech(techIds)
                .Apply();

            return result;

        }

        public async Task<ValidationResult> AddTech(SlackUserModel slackUserModel, IEnumerable<string> tech)
        {
            var slackUser = await _slackUserReader.Query().Where(k => k.UserId == slackUserModel.UserId).FirstOrDefaultAsync();
            if (slackUser == null)
            {
                throw new InvalidOperationException($"Could not find user {slackUserModel.UserName}");
            }
            var alreadySavedTech = slackUser.KnownTech.ToList();

            foreach (var maybeToAdd in tech)
            {
                if (!alreadySavedTech.Select(k => k.Name).Any(k => k == maybeToAdd))
                {
                    var techId = Guid.NewGuid();
                    await _techCommander.Add(techId)
                        .SetName(maybeToAdd)
                        .Apply();
                    alreadySavedTech.Add(_techReader.Query().First(k => k.TechId == techId));
                }
            }

            return await _slackUserCommander.Update(slackUser.SlackUserId)
                .AddKnownTech(alreadySavedTech.Select(k => k.TechId).ToArray())
                .Apply();

        }

        public async Task<IEnumerable<string>> ListTech()
        {
            return await _techReader.Query()
                .Select(k => k.Name + " (" + k.Smes.Count + ")")
                .ToListAsync();
        }

        public async Task<IEnumerable<string>> GetUsersWhoKnow(string tech)
        {
            var lowerTech = tech?.ToLower();
            return await _techReader.Query().SelectMany(k => k.Smes).Select(k => k.UserName).Distinct().ToListAsync();
                
        }
    }
    
}
