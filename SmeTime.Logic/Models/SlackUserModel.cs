﻿using System;

namespace SmeTime.Logic.Models
{
    public class SlackUserModel
    {
        public Guid SlackUserId { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string TeamId { get; set; }
        public string Token { get; set; }
    }
}
