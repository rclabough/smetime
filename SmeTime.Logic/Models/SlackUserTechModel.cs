﻿using System.Collections.Generic;

namespace SmeTime.Logic.Models
{
    public class SlackUserTechModel
    {
        public IEnumerable<string> KnownTech { get; set; }
    }
}