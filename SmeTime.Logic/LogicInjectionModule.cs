﻿using Ninject.Modules;
using Ninject.Extensions.Conventions;

namespace SmeTime.Logic
{
    public class LogicInjectionModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind(k => k.FromThisAssembly().SelectAllClasses().BindDefaultInterfaces());
        }
    }
}
