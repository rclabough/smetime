﻿using Ninject.Modules;
using Ninject.Extensions.Conventions;
using SmeTime.Data.Commanders;

namespace SmeTime.Data
{
    public class DataInjectionModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind(k => k.FromThisAssembly().SelectAllClasses().BindDefaultInterfaces());
            Kernel.Rebind<ISlackUserCommander>().To<SlackUsersCommander>();
        }
    }
}
