﻿using FluentValidation.Results;
using System.Threading.Tasks;
using System;
using System.Linq;
using FluentValidation;

namespace SmeTime.Data
{
    public interface IBaseCommand
    {
        ValidationResult Validate();
        Task<ValidationResult> Apply();
    }

    public class BaseCommand<T> : IBaseCommand where T : class, new()
    {
        private readonly IDbContext _dbContext;
        protected T Entity { get; set; }
        protected AbstractValidator<T> Validator { get; set; }

        public BaseCommand(IDbContext dbContext)
        {
            _dbContext = dbContext;
            Validator = new InlineValidator<T>();
        }

        protected IQueryable<S> Query<S>() where S : class, new()
        {
            return _dbContext.Query<S>();
        }

        protected void GenerateNew()
        {
            Entity = _dbContext.Add<T>();
        }

        public async Task<ValidationResult> Apply()
        {
            var validationResult = Validate();
            if (validationResult.IsValid)
            {
                await _dbContext.ApplyChangesAsync();
            }
            return validationResult;
        }

        public ValidationResult Validate()
        {
            return Validator.Validate(Entity);
        }
    }
}
