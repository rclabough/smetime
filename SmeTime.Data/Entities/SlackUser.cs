﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmeTime.Data.Entities
{
    public class SlackUser
    {
        public virtual Guid SlackUserId { get; set; }
        public virtual string Token { get; set; }

        [Index]
        [MaxLength(100)]
        public virtual string TeamId { get; set; }

        [Index]
        [MaxLength(100)]
        public virtual string UserId { get; set; }

        [Index]
        [MaxLength(100)]
        public virtual string UserName { get; set; }


        public virtual ICollection<Tech> KnownTech { get; set; }
    }
}
