﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmeTime.Data.Entities
{
    public class Tech
    {
        public virtual Guid TechId { get; set; }

        [MaxLength(100)]
        [Index(IsUnique = true)]
        public virtual string Name { get; set; }


        public virtual ICollection<SlackUser> Smes { get; set; }
    }
}
