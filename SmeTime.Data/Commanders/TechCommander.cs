﻿using FluentValidation;
using SmeTime.Data.Entities;
using System;
using System.Linq;

namespace SmeTime.Data.Commanders
{
    public interface ITechCommander
    {
        ITechSetCommand Add(Guid techId);
        ITechSetCommand Update(Guid techId);
    }

    public class TechCommander : ITechCommander
    {
        private readonly IDbContext _dbContext;

        public TechCommander(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public ITechSetCommand Add(Guid techId)
        {
            return TechSetCommand.Build(_dbContext, techId);
        }

        public ITechSetCommand Update(Guid techId)
        {
            return TechSetCommand.Build(_dbContext, techId);
        }
    }

    public interface ITechSetCommand : IBaseCommand
    {
        ITechSetCommand SetName(string name);
    }

    public class TechSetCommand : BaseCommand<Tech>, ITechSetCommand
    {

        public static TechSetCommand Build(IDbContext dbContext, Guid techId)
        {
            var cmd = new TechSetCommand(dbContext);
            cmd.Entity = cmd.Query<Tech>().FirstOrDefault(k => k.TechId == techId);
            if (cmd.Entity == null)
            {
                cmd.GenerateNew();
                cmd.Entity.TechId = techId;
            }
            return cmd;
        }

        public ITechSetCommand SetName(string name)
        {
            Validator.RuleFor(k => k.Name).NotEmpty();
            Entity.Name = name;
            return this;
        }

        private TechSetCommand(IDbContext dbContext) : base(dbContext)
        {
        }
    }
}
