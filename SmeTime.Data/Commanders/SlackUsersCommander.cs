﻿using FluentValidation;
using SmeTime.Data.Entities;
using SmeTime.Shared;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SmeTime.Data.Commanders
{
    public interface ISlackUserCommander
    {
        ISlackUserSetCommand Create();
        ISlackUserSetCommand Update(Guid slackUserId);
    }

    public class SlackUsersCommander : ISlackUserCommander
    {
        private IDbContext _dbContext;

        public SlackUsersCommander(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public ISlackUserSetCommand Create()
        {
            return SlackUserSetCommand.Create(_dbContext);
        }

        public ISlackUserSetCommand Update(Guid slackUserId)
        {
            return SlackUserSetCommand.Update(_dbContext, slackUserId);
        }
    }

    public interface ISlackUserSetCommand : IBaseCommand
    {
        ISlackUserSetCommand SetToken(string token);
        ISlackUserSetCommand SetTeamId(string teamId);
        ISlackUserSetCommand SetUserId(string userId);
        ISlackUserSetCommand SetUserName(string userName);
        ISlackUserSetCommand SetKnownTech(IEnumerable<Guid> knownTechIds);
        ISlackUserSetCommand AddKnownTech(params Guid[] knownTechIds);
    }

    public class SlackUserSetCommand : BaseCommand<SlackUser>, ISlackUserSetCommand
    {
        private SlackUserSetCommand(IDbContext dbContext) : base(dbContext)
        {
        }

        public static SlackUserSetCommand Create(IDbContext dbContext)
        {
            var cmd = new SlackUserSetCommand(dbContext);
            cmd.GenerateNew();
            cmd.Entity.SlackUserId = Guid.NewGuid();
            return cmd;
        }

        public static SlackUserSetCommand Update(IDbContext dbContext, Guid slackUserId)
        {
            var cmd = new SlackUserSetCommand(dbContext);
            cmd.Entity = cmd.Query<SlackUser>().FirstOrDefault(k => k.SlackUserId == slackUserId);
            return cmd;
        }

        private void InitializeTechIfNeeded()
        {
            if (Entity.KnownTech == null)
            {
                Entity.KnownTech = new List<Tech>();
            }
        }

        public ISlackUserSetCommand AddKnownTech(params Guid[] knownTechIds)
        {
            InitializeTechIfNeeded();

            var toAdd = Query<Tech>().Where(k => knownTechIds.Contains(k.TechId)).ToList();

            Entity.KnownTech.AddRange(toAdd);

            return this;
        }

        public ISlackUserSetCommand SetKnownTech(IEnumerable<Guid> knownTechIds)
        {
            InitializeTechIfNeeded();
            Entity.KnownTech.Clear();
            var tech = Query<Tech>().Where(k => knownTechIds.Contains(k.TechId)).ToList();

            Entity.KnownTech.AddRange(tech);

            return this;
        }

        public ISlackUserSetCommand SetTeamId(string teamId)
        {
            Validator.RuleFor(k => k.TeamId).NotEmpty();
            Entity.TeamId = teamId;
            return this;
        }

        public ISlackUserSetCommand SetToken(string token)
        {
            Validator.RuleFor(k => k.Token).NotEmpty();
            Entity.Token = token;
            return this;
        }

        public ISlackUserSetCommand SetUserId(string userId)
        {
            Validator.RuleFor(k => k.UserId).NotEmpty();
            Entity.UserId = userId;
            return this;
        }

        public ISlackUserSetCommand SetUserName(string userName)
        {
            Validator.RuleFor(k => k.UserName).NotEmpty();
            Entity.UserName = userName;
            return this;
        }
    }
}
