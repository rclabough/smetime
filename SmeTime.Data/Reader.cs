﻿using System.Linq;

namespace SmeTime.Data
{
    public class Reader<T> : IReader<T> where T : class, new()
    {
        private IDbContext _dbContext;

        public Reader(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<T> Query()
        {
            return _dbContext.Query<T>();
        }
    }
}
