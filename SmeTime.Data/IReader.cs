﻿using System.Linq;

namespace SmeTime.Data
{
    public interface IReader<T> where T : class, new()
    {
        IQueryable<T> Query();
    }
}
