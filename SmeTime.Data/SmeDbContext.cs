﻿using LinqKit;
using SmeTime.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmeTime.Data
{
    public class SmeDbContext : DbContext, IDbContext
    {
        static SmeDbContext()
        {
            var ensureDLLIsCopied = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }

        public virtual DbSet<Tech> Tech { get; set; }

        public virtual DbSet<SlackUser> SlackUser { get; set; }


        public SmeDbContext() : base("SmeDb")
        {
            
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Tech>().HasKey(k => k.TechId);
            modelBuilder.Entity<Tech>().HasMany(k => k.Smes).WithMany(k => k.KnownTech);

            modelBuilder.Entity<SlackUser>().HasKey(k => k.SlackUserId);
            modelBuilder.Entity<SlackUser>().HasMany(k => k.KnownTech).WithMany(k => k.Smes);
        }

        public T Add<T>() where T : class, new()
        {
            var entity = new T();
            Set<T>().Add(entity);
            return entity;
        }

        public IQueryable<T> Query<T>() where T : class, new()
        {
            return Set<T>().AsExpandable();
        }

        public async Task ApplyChangesAsync()
        {
            await SaveChangesAsync();
        }
    }
}
