namespace SmeTime.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initialmigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SlackUsers",
                c => new
                    {
                        SlackUserId = c.Guid(nullable: false),
                        Token = c.String(),
                        TeamId = c.String(maxLength: 100),
                        UserId = c.String(maxLength: 100),
                        UserName = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.SlackUserId)
                .Index(t => t.TeamId)
                .Index(t => t.UserId)
                .Index(t => t.UserName);
            
            CreateTable(
                "dbo.Teches",
                c => new
                    {
                        TechId = c.Guid(nullable: false),
                        Name = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.TechId)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.SlackUserTeches",
                c => new
                    {
                        SlackUser_SlackUserId = c.Guid(nullable: false),
                        Tech_TechId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.SlackUser_SlackUserId, t.Tech_TechId })
                .ForeignKey("dbo.SlackUsers", t => t.SlackUser_SlackUserId, cascadeDelete: true)
                .ForeignKey("dbo.Teches", t => t.Tech_TechId, cascadeDelete: true)
                .Index(t => t.SlackUser_SlackUserId)
                .Index(t => t.Tech_TechId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SlackUserTeches", "Tech_TechId", "dbo.Teches");
            DropForeignKey("dbo.SlackUserTeches", "SlackUser_SlackUserId", "dbo.SlackUsers");
            DropIndex("dbo.SlackUserTeches", new[] { "Tech_TechId" });
            DropIndex("dbo.SlackUserTeches", new[] { "SlackUser_SlackUserId" });
            DropIndex("dbo.Teches", new[] { "Name" });
            DropIndex("dbo.SlackUsers", new[] { "UserName" });
            DropIndex("dbo.SlackUsers", new[] { "UserId" });
            DropIndex("dbo.SlackUsers", new[] { "TeamId" });
            DropTable("dbo.SlackUserTeches");
            DropTable("dbo.Teches");
            DropTable("dbo.SlackUsers");
        }
    }
}
