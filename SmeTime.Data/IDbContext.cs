﻿using System.Linq;
using System.Threading.Tasks;

namespace SmeTime.Data
{
    public interface IDbContext
    {
        T Add<T>() where T : class, new();
        IQueryable<T> Query<T>() where T : class, new();
        Task ApplyChangesAsync();
    }
}
