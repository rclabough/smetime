﻿using System;
using System.Configuration;

namespace SmeTime.Data.Configuration
{
    public interface IConfigurationManager
    {
    }

    public class Configuration : IConfigurationManager
    {

        private string GetConfigValue(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }
        
    }
}
