﻿using FluentValidation.Results;
using Newtonsoft.Json;
using SmeTime.Logic.Interfaces;
using SmeTime.Logic.Models;
using SmeTime.Models;
using SmeTime.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace SmeTime.Controllers
{
    public class SlackController : ApiController
    {
        private ISmeService _smeService;

        public SlackController(ISmeService smeService)
        {
            _smeService = smeService;
        }

        [HttpGet]
        public async Task<IHttpActionResult> Test(string token,
            string team_id,
            string channel_id,
            string channel_name,
            string user_id,
            string user_name,
            string command,
            string text)
        {
            var slackRequestModel = new SlackRequestModel
            {
                team_id = team_id,
                token = token,
                channel_id = channel_id,
                channel_name = channel_name,
                user_id = user_id,
                user_name = user_name,
                command = command,
                text = text
            };
            try
            {
                return await ProcessRequest(slackRequestModel);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> Index(SlackRequestModel slackRequestModel)
        {
            try
            {
                return await ProcessRequest(slackRequestModel);
            }
            catch (Exception ex)
            {
                // Slack requires an 'OK' response
                return Ok(new { text = ex.ToFormattedMessage() });
            }
        }

        private async Task<IHttpActionResult> ProcessRequest(SlackRequestModel slackRequestModel)
        {
            if (slackRequestModel == null || slackRequestModel.text == null)
            {
                throw new NullReferenceException("Text was null");
            }

            var parsedText = slackRequestModel.text.Split(' ').ToList();

            var command = parsedText[0];

            var slackUserModel = ParseSlackUserModel(slackRequestModel);

            var subcommands = parsedText.Skip(1).ToList();

            switch (command.ToLower())
            {
                case "register":
                    var result = await _smeService.RegisterTech(slackUserModel, subcommands);
                    return SerializeResult(result);
                case "lookup":
                    var tech = subcommands.FirstOrDefault();
                    var knownTech = await _smeService.GetUsersWhoKnow(tech);
                    var csstring = string.Join(", ", knownTech);
                    if(knownTech.Count() > 0)
                    {
                        return SerializeResult($"Looks like  these people know about {tech}: {csstring}");
                    }
                    else
                    {
                        return SerializeResult($"Seems like no one knows about {tech}!");
                    }
                    
                case "add":
                    var addResult = await _smeService.AddTech(slackUserModel, subcommands);
                    return SerializeResult(addResult);
                case "list":
                    var listResult = await _smeService.ListTech();
                    return SerializeResult(listResult);
                case "help":
                    return SerializeResult(Help());
                default:
                    throw new InvalidOperationException($"Unknown command: {command}");
            }
        }

        private IHttpActionResult SerializeResult(ValidationResult validationResult)
        {
            if (validationResult.IsValid)
            {
                return SerializeResult("Change Successful");
            }
            else
            {
                return Ok(new { text = validationResult.ToException() });
            }
        }

        private IHttpActionResult SerializeResult(IEnumerable<string> listOfThings)
        {
            return SerializeResult(listOfThings.ToFormattedString());
        }

        private IHttpActionResult SerializeResult(string str)
        {
            return Ok(new { text = str });
        }

        private IHttpActionResult SerializeResult<T>(T input)
        {
            return Ok(new { text = JsonConvert.SerializeObject(input) });
        }

        private SlackUserModel ParseSlackUserModel(SlackRequestModel slackRequestModel)
        {
            return new SlackUserModel
            {
                UserId = slackRequestModel.user_id,
                UserName = slackRequestModel.user_name,
                Token = slackRequestModel.token,
                TeamId = slackRequestModel.team_id,
            };
        }

        private class SlackFunctionModel
        {
            public Action Action { get; set; }
            public SlackUserModel SlackUserModel { get; set; }
        }

        private IEnumerable<string> Help()
        {
            var list = new List<string>();
            list.Add(FormatHelpMessage("register", "/register java csharp c++", "Register yourself as a SME in these technologies (Will delete all techs if used twice)"));
            list.Add(FormatHelpMessage("lookup", "/lookup java", "Find users who know _java_ technology"));
            list.Add(FormatHelpMessage("list", "/list", "List all technologies in the system"));
            list.Add(FormatHelpMessage("add", "/add angular", "Adds _angular_ to your list of technologies"));
            return list;
        }

        private string FormatHelpMessage(string command, string example_command, string message)
        {
            return $"*{command}:* `{example_command}` - {message}";
        }
    }
}
