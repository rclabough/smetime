﻿using Ninject.Modules;
using Ninject.Extensions.Conventions;

namespace SmeTime
{
    public class ApiInjectionModule : NinjectModule
    {
        
        public override void Load()
        {
            Kernel.Bind(k => k.FromThisAssembly().SelectAllClasses().BindDefaultInterfaces());
        }
        
    }
}